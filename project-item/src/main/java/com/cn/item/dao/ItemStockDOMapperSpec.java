package com.cn.item.dao;

import com.cn.item.dao.data.ItemStockDO;

public interface ItemStockDOMapperSpec {

    ItemStockDO selectByItemId(Integer itemId);

    void updateStockByItemId(ItemStockDO itemStockDO);
}
