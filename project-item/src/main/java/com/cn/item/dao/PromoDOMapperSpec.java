package com.cn.item.dao;

import com.cn.item.dao.data.PromoDO;

public interface PromoDOMapperSpec {
    PromoDO selectByItemId(Integer itemId);
}
