package com.cn.item.exception;


//包装器业务异常类实现
public class BusinessException extends Exception implements com.cn.item.exception.CommonError {

    private com.cn.item.exception.CommonError commonError;

    //直接接收EmBusinessError的传参用于构造业务异常
    public BusinessException(com.cn.item.exception.CommonError commonError){
        super();
        this.commonError = commonError;
    }

    //接收自定义errMsg的方式构造业务异常
    public BusinessException(com.cn.item.exception.CommonError commonError, String errMsg){
        super();
        this.commonError = commonError;
        this.commonError.setErrMsg(errMsg);
    }





    @Override
    public int getErrCode() {
        return this.commonError.getErrCode();
    }

    @Override
    public String getErrMsg() {
        return this.commonError.getErrMsg();
    }

    @Override
    public com.cn.item.exception.CommonError setErrMsg(String errMsg) {
        this.commonError.setErrMsg(errMsg);
        return this;
    }

    public com.cn.item.exception.CommonError getCommonError() {
        return commonError;
    }
}
