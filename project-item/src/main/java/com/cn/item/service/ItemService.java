package com.cn.item.service;

import com.cn.item.exception.BusinessException;
import com.cn.item.model.ItemModel;

public interface ItemService {

    //创建商品
    ItemModel createItem(ItemModel itemModel) throws BusinessException;

    //更新商品
    ItemModel updateItem(ItemModel itemModel) throws BusinessException;


    //商品详情浏览
    ItemModel getItemById(Integer id);


}
